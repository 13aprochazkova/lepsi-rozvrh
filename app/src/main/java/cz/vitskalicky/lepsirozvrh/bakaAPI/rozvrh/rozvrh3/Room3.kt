package cz.vitskalicky.lepsirozvrh.bakaAPI.rozvrh.rozvrh3

data class Room3 (
    var id: String,
    var abbrev: String,
    var name: String,
)