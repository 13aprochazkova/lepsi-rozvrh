package cz.vitskalicky.lepsirozvrh.bakaAPI.rozvrh.rozvrh3

data class Hour3 (
    val id: Int,
    val caption: String,
    val beginTime: String,
    val endTime: String,
)