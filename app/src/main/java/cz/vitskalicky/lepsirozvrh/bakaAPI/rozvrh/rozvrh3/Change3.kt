package cz.vitskalicky.lepsirozvrh.bakaAPI.rozvrh.rozvrh3

data class Change3 (
    val changeSubject: String?,
    val day: String?,
    val hours: String?,
    val changeType: String?,
    val description: String?,
    val time: String?,
    val typeAbbrev: String?,
    val typeName: String?,
)