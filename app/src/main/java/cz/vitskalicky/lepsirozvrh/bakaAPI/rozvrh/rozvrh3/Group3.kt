package cz.vitskalicky.lepsirozvrh.bakaAPI.rozvrh.rozvrh3

data class Group3 (
    val classId: String,
    val id: String,
    val abbrev: String,
    val name: String,
)