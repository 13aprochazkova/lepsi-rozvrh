package cz.vitskalicky.lepsirozvrh.bakaAPI.rozvrh.rozvrh3

data class Cycle3 (
    val id: String,
    val abbrev: String,
    val name: String,
)